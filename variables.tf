variable "organization" {}
variable "province" {}
variable "locality" {}

variable "root_ca_private_key_file" {
  default = ""
}

variable "root_ca_certificate_file" {
  default = ""
}
