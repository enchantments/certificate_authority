resource "tls_private_key" "root_authority" {
  count       = "${var.root_ca_private_key_file == "" ? 1 : 0}"

  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_self_signed_cert" "root_authority" {
  count       = "${var.root_ca_certificate_file == "" ? 1 : 0}"

  key_algorithm   = "ECDSA"
  private_key_pem = "${tls_private_key.root_authority[0].private_key_pem}"

  subject {
    common_name  = "Root Authority"
    organization  = var.organization
    province      = var.province
    locality      = var.locality
  }

  validity_period_hours = 72

  is_ca_certificate = true

  allowed_uses = [
    "cert_signing",
    "crl_signing",
  ]
}
