resource "tls_private_key" "intermediate_authority" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "intermediate_authority" {
  key_algorithm   = "ECDSA"
  private_key_pem = "${tls_private_key.intermediate_authority.private_key_pem}"

  subject {
    common_name  = "Intermediate Authority"
    organization  = var.organization
    province      = var.province
    locality      = var.locality
  }
}

resource "tls_locally_signed_cert" "intermediate_authority" {
  cert_request_pem      = "${tls_cert_request.intermediate_authority.cert_request_pem}"
  ca_private_key_pem    = "${var.root_ca_private_key_file != "" ? file(var.root_ca_private_key_file) : tls_private_key.root_authority[0].private_key_pem}"
  ca_cert_pem           = "${var.root_ca_certificate_file != "" ? file(var.root_ca_certificate_file) : tls_self_signed_cert.root_authority[0].cert_pem}"
  ca_key_algorithm      = "ECDSA"

  is_ca_certificate     = true

  validity_period_hours = 72

  allowed_uses = [
    "cert_signing",
    "crl_signing",
  ]
}
