output "intermediate_private_key_pem" {
  value     = tls_private_key.intermediate_authority.private_key_pem
  sensitive = true
}

output "intermediate_certificate_pem" {
  value = tls_locally_signed_cert.intermediate_authority.cert_pem
}

output "root_private_key_pem" {
  value     = var.root_ca_private_key_file != "" ? file(var.root_ca_private_key_file) : tls_private_key.root_authority[0].private_key_pem
  sensitive = true
}

output "root_certificate_pem" {
  value = var.root_ca_certificate_file != "" ? file(var.root_ca_certificate_file) : tls_self_signed_cert.root_authority[0].cert_pem
}

output "certificate_chain_pem" {
  value = format("%s%s" , tls_locally_signed_cert.intermediate_authority.cert_pem, var.root_ca_certificate_file != "" ? file(var.root_ca_certificate_file) : tls_self_signed_cert.root_authority[0].cert_pem)
}

output "organization" { value = var.organization }
output "province" { value = var.province }
output "locality" { value = var.locality }
